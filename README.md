# noark-xmp
Working with Noark 5 metadata and embed it in a PDF-document.

Currently we are able to read metadata from a PDF-file. This includes both 
PDF-metadata as well as XMP-metadata.

## Requiremnets 

    java 1.8
    maven

## Download
Assuming you have a ~/git directory, cd into ~/git. Download or clone the git
repository

    git clone https://github.com/HiOA-ABI/noark-xmp


## Install 
cd into the correct directory cd noark-xmp

    mvn compile
    mvn package

Assuming you are in the noark-xmp directory, you can copy the included 
PDF-file (PDF/A from LibreOffice) to the root so that you can use it.

    cp src/main/resources/pdf_files/pdf_little_metadata.pdf .


Then you can run the code on the command line:

    java -cp target/noark-xmp-1.0-SNAPSHOT.jar oslomet.noark.xmp.Main --command getter --file pdf_little_metadata.pdf        

Running the above command will show extracted metadata. You will see there is
little metadata in the PDF file. Running the following command will overwrite
some of that metadata and add some additional metadata:  
    
    java -cp target/noark-xmp-1.0-SNAPSHOT.jar oslomet.noark.xmp.Main --command setter --file pdf_little_metadata.pdf

Running the original getter command (below) will again show you all the 
metadata, but this time there is a lot more metadata.
    
    java -cp target/noark-xmp-1.0-SNAPSHOT.jar oslomet.noark.xmp.Main --command getter --file pdf_little_metadata.pdf

The code is very experimental and will probably have issues with various PDF 
files. So far we have seen differences between FrameMaker and LibreOffice as 
PDF producer that the code had issues handling. A single test of a PDF file
created from MS Office 2010 showed missing XMP metadata. So there are many 
variations with regards to the amount of metadata that various PDF producing 
tools support.

Future work includes making the handling of RDF more robust and do start the 
development of Noark RDF that we can add Noark metadata to a PDF file.

We are currently using jena to parse the RDF. This is a little problematic as
the RDF is encapsulated in <xpacket> and <xmpmeta> tags so we are removing 
these tags with String.replaceAll calls. We would probably be better off 
designing a antlr4 grammar so we can control the whole process. jena also 
seems to be a largish framework to invoke for every file to be processed. But
to just test out the concept it's OK.

There also seems to be multiple namespaces at play
http://ns.adobe.com/xap/1.0/ and http://ns.adobe.com/xap/1.0/mm/ appear to be
the same namespace, but I'm not entirely sure. This needs to be checked. It 
causes problems when handling the RDF that probably could be solved by 
removing the mm part.

The code is reporting that it's unable to handle some RDF predicates. Some 
metadata is obtained by traversing to another statement / node. This is shown
as:

    Error handling http://www.w3.org/1999/02/22-rdf-syntax-ns#_1    
    Error handling http://www.w3.org/1999/02/22-rdf-syntax-ns#type
    Error handling http://www.w3.org/1999/02/22-rdf-syntax-ns#_1
    Error handling http://www.w3.org/1999/02/22-rdf-syntax-ns#type

We will deal with this later when we start working on how to better handle 
the RDF.

Note: I am not sure but it seems that the updated XMP metadata has an XML 
declaration rather than a xpacket node. This should be checked further.
