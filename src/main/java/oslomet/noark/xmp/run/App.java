package oslomet.noark.xmp.run;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import oslomet.noark.xmp.controller.MainController;

/**

 */

public class App extends Application {

    private Stage stage;
    private FXMLLoader loader;
    private BorderPane rootNode;
    private static String [] savedArgs;

    public Stage getMainStage() {
        return stage;
    }

    @Override
    public void start(Stage stage) throws Exception {

            stage.setTitle("XMP Manipulator (Trondheim Kommune / OsloMet)");
            stage.setScene(new Scene(rootNode, 800, 600));

            // Give the controller access to the main app
            MainController controller = loader.getController();
            controller.setMainApp(this);
            stage.show();
    }

    @Override
    public void init() throws Exception {
        loader = new FXMLLoader(getClass().getResource("main.fxml"));
        rootNode = loader.load();
    }

    public static void main(String[] args) {
        savedArgs = args;
        launch(App.class, args);
    }
}
