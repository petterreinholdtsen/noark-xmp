package oslomet.noark.xmp.model;

public class MetadataObjectDescription {

    private String namespace;
    private String name;
    private String type;

    public MetadataObjectDescription(String namespace, String name, String type) {
        this.namespace = namespace;
        this.name = name;
        this.type = type;
    }

    public String getNamespace() {
        return namespace;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }
}
