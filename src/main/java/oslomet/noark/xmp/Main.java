package oslomet.noark.xmp;

import org.apache.commons.cli.*;
import org.apache.jempbox.xmp.XMPMetadata;
import org.apache.jempbox.xmp.XMPSchemaBasic;
import org.apache.jempbox.xmp.XMPSchemaDublinCore;
import org.apache.jempbox.xmp.XMPSchemaPDF;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import oslomet.noark.xmp.model.MetadataObject;

import java.io.File;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;
import java.util.Set;

import static java.lang.System.out;

/**
 * noark-xmp
 * <p>
 * An example on how to extract metadata from a PDF file, both PDF and XMP
 * metadata. This example will continue to be updated as we progress to be
 * able to insert Noark metadata in PDF file.
 * <p>
 * Note this retrieves metadata from a file called pdf_little_metadata.pdf in
 * the resources directory. Remember when running it will work on a copy of
 * the file in the target directory.
 * <p>
 * Output is styled on exif output
 */
public class Main {

    public static void main(String[] args) throws Exception {

        Options options = new Options();
        options.addOption(Option
                .builder()
                .hasArg()
                .optionalArg(false)
                .desc("command has to be either 'getter' or 'setter'")
                .argName("command")
                .longOpt("command")
                .build()
        );
        options.addOption(Option
                .builder()
                .hasArg()
                .longOpt("file")
                .optionalArg(false)
                .desc("path to PDF-file")
                .argName("file")
                .build()
        );
        CommandLineParser parser = new DefaultParser();
        CommandLine commandLine = parser.parse(options, args);

        String command = commandLine.getOptionValue("command");
        String filename = commandLine.getOptionValue("file");

        out.println("Starting noark-xmp");


        Main main = new Main();

        if (command.equalsIgnoreCase("getter")) {
            main.runGetter(filename);
        } else if (command.equalsIgnoreCase("setter")) {
            main.runSetter(filename);
        }
        out.println("Finished noark-xmp");
    }

    private void runGetter(String filename) throws Exception {
        File file = new File(filename);

        PDDocument pdfDoc = PDDocument.load(file);

        if (pdfDoc.isEncrypted()) {
            out.println("Cannot process document as it is encrypted!");
            return;
        }

        try {

            XMPGetter xmpGetter = new XMPGetter(pdfDoc);

            out.println("The following Document Information metadata was " +
                    "found.");

            String format = "%-45s%s%n";

            // Get and print out PDF-defined metadata
            PDDocumentInformation info = xmpGetter.getDocumentInformation();
            Set<String> keys = info.getMetadataKeys();

            for (String key : keys) {
                out.printf(format, "[PDF]\t  " + key, ": \t" + info
                        .getCustomMetadataValue(key));
            }

            // Get and print out XMP defined metata
            Map<String, MetadataObject> metadataObjectMap = xmpGetter
                    .getXMPMetadata();

            for (Map.Entry entry : metadataObjectMap.entrySet()) {
                MetadataObject currentObject = (MetadataObject) entry.getValue();
                out.printf(format,
                        "[" + currentObject.getType() +
                                "]\t  " + currentObject.getName(),
                        ": \t" + currentObject.getValue()
                );
            }
        } catch (Exception e) {
            out.println("Error when processing metadata in " + file.toString());
            e.printStackTrace();
        } finally {
            if (pdfDoc != null) {
                try {
                    pdfDoc.close();
                } catch (Throwable err2) {
                }
            }
        }
    }

    /**
     * main
     * <p>
     * Just an example of how to update and add metadata to PDF document.
     * <p>
     * Note! The code runs on the contents of the target directory, not the
     * src directory.
     * <p>
     * The file pdf_no_metadata.pdf in the resources directory contains little
     * metadata. This can be verified by running
     * <p>
     * exiftool -a -G1  pdf_little_metadata.pdf
     * <p>
     * The following is a list of metadata items contained in the file:
     * <p>
     * [PDF]           PDF Version                     : 1.4
     * [PDF]           Linearized                      : No
     * [PDF]           Page Count                      : 1
     * [PDF]           Language                        : en-GB
     * [PDF]           Tagged PDF                      : Yes
     * [PDF]           Creator                         : Writer
     * [PDF]           Producer                        : LibreOffice 5.4
     * [PDF]           Create Date                     : 2018:03:02 09:55:12+01:00
     * [XMP-pdfaid]    Part                            : 1
     * [XMP-pdfaid]    Conformance                     : A
     * [XMP-pdf]       Producer                        : LibreOffice 5.4
     * [XMP-xmp]       Creator Tool                    : Writer
     * [XMP-xmp]       Create Date                     : 2018:03:02 09:55:12+01:00
     * <p>
     * After you run main, you can re-run the exiftool :
     * <p>
     * exiftool -a -G1  pdf_little_metadata.pdf
     * <p>
     * This should produce the following metadata:
     * [PDF]           PDF Version                     : 1.4
     * [PDF]           Linearized                      : No
     * [PDF]           Page Count                      : 1
     * [PDF]           Language                        : en-GB
     * [PDF]           Tagged PDF                      : Yes
     * [PDF]           Creator                         : Writer
     * [PDF]           Producer                        : LibreOffice 5.4
     * [PDF]           Create Date                     : 2018:03:02 09:55:12+01:00
     * [XMP-pdf]       Keywords                        : XMP Metadata Noark-XMP
     * [XMP-pdf]       Producer                        : noark-xmp edit tool producer
     * [XMP-rdf]       About                           : Testing noark-xmp
     * [XMP-xmp]       Base URL                        : https://github.com/HiOA-ABI/noark-xmp
     * [XMP-xmp]       Create Date                     : 2018:03:02 10:59:44+01:00
     * [XMP-xmp]       Creator Tool                    : noark-xmp edit tool creator
     * [XMP-xmp]       Label                           : noark-xmp label
     * [XMP-xmp]       Metadata Date                   : 2018:03:02 10:59:44+01:00
     * [XMP-xmp]       Modify Date                     : 2018:03:02 10:59:44+01:00
     * [XMP-xmp]       Nickname                        : noark-xmp nickname
     * [XMP-xmp]       Title                           : Testing adding metadata
     * [XMP-dc]        Title                           : dc: title
     * [XMP-dc]        Creator                         : dc: creator
     * [XMP-dc]        Description                     : dc: description
     * [XMP-dc]        Coverage                        : dc: coverage
     * [XMP-dc]        Format                          : dc: format
     * [XMP-dc]        Rights                          : dc: rights
     * [XMP-dc]        Identifier                      : dc: identifier
     * [XMP-dc]        Source                          : dc: source
     * <p>
     * So here you can see that some metadata (e.g. Keywords, Producer) have
     * been updated and a lot of new metadata items have been introduced.
     *
     * @throws Exception
     */

    private void runSetter(String filename) throws Exception {

        File file = new File(filename);

        PDDocument pdfDoc = PDDocument.load(file);

        if (pdfDoc.isEncrypted()) {
            out.println("Cannot process document as it is encrypted!");
            return;
        }

        XMPSetter xmpSetter = new XMPSetter(pdfDoc);

        // Pick up the date for use on the PDF-file
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(new Date());

        // Set PDF metadata

        // Set XMP-PDF metadata
        XMPMetadata metadata = new XMPMetadata();
        XMPSchemaPDF pdfSchema = metadata.addPDFSchema();
        pdfSchema.setKeywords("XMP Metadata Noark-XMP");
        pdfSchema.setProducer("noark-xmp edit tool producer");
        pdfSchema.setTextProperty("Test-hello", "hello");
        XMPSchemaBasic basicSchema = metadata.addBasicSchema();
        basicSchema.setModifyDate(cal);
        basicSchema.setCreateDate(cal);
        basicSchema.setCreatorTool("noark-xmp edit tool creator");
        basicSchema.setMetadataDate(cal);
        basicSchema.setTitle("Testing adding metadata");
        basicSchema.setBaseURL("https://github.com/HiOA-ABI/noark-xmp");
        basicSchema.setLabel("noark-xmp label");
        basicSchema.setNickname("noark-xmp nickname");
        XMPSchemaDublinCore dcSchema = metadata.addDublinCoreSchema();
        dcSchema.setTitle("dc: title");
        dcSchema.addCreator("dc: creator");
        dcSchema.setDescription("dc: description");
        dcSchema.setCoverage("dc: coverage");
        dcSchema.setFormat("dc: format");
        dcSchema.setRights("dc: rights");
        dcSchema.setIdentifier("dc: identifier");
        dcSchema.setSource("dc: source");

        xmpSetter.setMetadata(metadata);
        pdfDoc.save(file);
        pdfDoc.close();
    }


}
