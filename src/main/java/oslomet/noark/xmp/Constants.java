package oslomet.noark.xmp;

public final class Constants {

    public static final String XMP_DC  = "XMP-dc";
    public static final String XMP_NOARK  = "XMP-noark";
    public static final String XMP_PDF  = "XMP-pdf";
    public static final String XMP_PDFA = "XMP-pdfaid";
    public static final String XMP_PDFX = "XMP-pdfx";
    public static final String XMP_XAP  = "XMP-xap";

    public static final String BASE_URL  = "baseurl";
    public static final String CREATE_DATE  = "createdate";
    public static final String CREATOR = "creator";
    public static final String CREATOR_TOOL = "creatortool";
    public static final String COPYRIGHT = "copyright";
    public static final String CONFORMANCE = "conformance";
    public static final String COVERAGE = "coverage";
    public static final String DESCRIPTION = "description";
    public static final String DOCUMENT_ID = "documentid";
    public static final String FORMAT = "format";
    public static final String LABEL = "label";
    public static final String INSTANCE_ID = "instanceid";
    public static final String IDENTIFIER = "identifier";
    public static final String KEYWORDS = "keywords";
    public static final String METADATA_DATE = "metadatadate";
    public static final String MODIFY_DATE = "modifydate";
    public static final String NICKNAME = "nickname";
    public static final String PART = "part";
    public static final String PRODUCER = "producer";
    public static final String RIGHTS = "rights";
    public static final String SOURCE = "source";
    public static final String TITLE = "title";

    public static final String NS_DC_1_1 = "http://purl.org/dc/elements/1.1/";
    public static final String NS_PDF_1_3 = "http://ns.adobe.com/pdf/1.3/";
    public static final String NS_PDFA = "http://www.aiim.org/pdfa/ns/id/";
    public static final String NS_PDFX_1_3 = "http://ns.adobe.com/pdfx/1.3/";;
    public static final String NS_XAP_1_0 = "http://ns.adobe.com/xap/1.0/";
    public static final String NS_XAP_1_0_MM = "http://ns.adobe.com/xap/1.0/mm/";

    public static final String NS_DC_1_1_CREATOR =
            NS_DC_1_1 + CREATOR;

    public static final String NS_DC_1_1_COVERAGE =
            NS_DC_1_1 + COVERAGE;

    public static final String NS_DC_1_1_DESCRIPTION =
            NS_DC_1_1 + DESCRIPTION;

    public static final String NS_DC_1_1_FORMAT =
            NS_DC_1_1 + FORMAT;

    public static final String NS_DC_1_1_IDENTIFIER =
            NS_DC_1_1 + IDENTIFIER;

    public static final String NS_DC_1_1_SOURCE =
            NS_DC_1_1 + SOURCE;

    public static final String NS_DC_1_1_RIGHTS =
            NS_DC_1_1 + RIGHTS;

    public static final String NS_DC_1_1_TITLE =
            NS_DC_1_1 + TITLE;


    public static final String NS_PDF_1_3_KEYWORDS =
            NS_PDF_1_3 + KEYWORDS;

    public static final String NS_PDF_1_3_PRODUCER =
            NS_PDF_1_3 + PRODUCER;

    public static final String NS_PDFX_1_3_COPYRIGHT =
            NS_PDFX_1_3 + COPYRIGHT;

    public static final String NS_PDFA_CONFORMANCE =
            NS_PDFA + CONFORMANCE;

    public static final String NS_PDFA_PART =
            NS_PDFA + PART;

    // http://ns.adobe.com/xap/1.0/

    public static final String NS_XAP_1_0_BASE_URL =
            NS_XAP_1_0 + BASE_URL;

    public static final String NS_XAP_1_0_CREATE_DATE =
            NS_XAP_1_0 + CREATE_DATE;

    public static final String NS_XAP_1_0_CREATOR_TOOL =
            NS_XAP_1_0 + CREATOR_TOOL;

    public static final String NS_XAP_1_0_LABEL =
            NS_XAP_1_0 + LABEL;

    public static final String NS_XAP_1_0_METADATA_DATE =
            NS_XAP_1_0 + METADATA_DATE;

    public static final String NS_XAP_1_0_MODIFY_DATE =
            NS_XAP_1_0 + MODIFY_DATE;

    public static final String NS_XAP_1_0_NICKNAME =
            NS_XAP_1_0 + NICKNAME;

    public static final String NS_XAP_1_0_TITLE =
            NS_XAP_1_0 + TITLE;

    // http://ns.adobe.com/xap/1.0/

    public static final String NS_XAP_1_0_BASE_URL_MM =
            NS_XAP_1_0_MM + BASE_URL;

    public static final String NS_XAP_1_0_TITLE_MM =
            NS_XAP_1_0_MM + TITLE;

    public static final String NS_XAP_1_0_MODIFY_DATE_MM =
            NS_XAP_1_0_MM + MODIFY_DATE;

    public static final String NS_XAP_1_0_LABEL_MM =
            NS_XAP_1_0_MM + LABEL;



    public static final String NS_XAP_1_0_DOCUMENT_ID_MM =
            NS_XAP_1_0_MM + DOCUMENT_ID;

    public static final String NS_XAP_1_0_INSTANCE_ID_MM =
            NS_XAP_1_0_MM + INSTANCE_ID;

}
