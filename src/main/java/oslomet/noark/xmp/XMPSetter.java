package oslomet.noark.xmp;

import org.apache.jempbox.xmp.XMPMetadata;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.common.PDMetadata;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayOutputStream;

/**
 * XMPSetter
 * <p>
 * Implements basic functionality to write XMP metadata to a identified and
 * opened PDF file.
 */
public class XMPSetter {

    private PDDocument pdfDoc;

    public XMPSetter(PDDocument pdfDoc) {
        this.pdfDoc = pdfDoc;
    }

    /**
     * setMetadata
     *
     * @param metadata The metadata object that you wish to be applied to the
     *                 PDF file
     * @throws Exception Various exceptions that can be thrown. Caller has to
     *                   deal with any problems and reattempt.
     */
    public void setMetadata(XMPMetadata metadata) throws Exception {

        PDMetadata metadataStream = new PDMetadata(pdfDoc);
        PDDocumentCatalog catalog = pdfDoc.getDocumentCatalog();

        // XMP metadata can only be written using a byte stream, so we have
        // to convert our XML document of XMP RDF metadata to a byte stream
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        Source xmlSource = new DOMSource(metadata.getXMPDocument());
        Result outputTarget = new StreamResult(outputStream);
        TransformerFactory.newInstance().newTransformer().
                transform(xmlSource, outputTarget);
        metadataStream.importXMPMetadata(outputStream.toByteArray());
        catalog.setMetadata(metadataStream);
    }
}
