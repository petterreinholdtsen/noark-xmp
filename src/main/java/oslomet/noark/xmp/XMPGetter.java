package oslomet.noark.xmp;

import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFParser;
import org.apache.pdfbox.io.IOUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.pdmodel.common.PDMetadata;
import org.apache.jena.sparql.util.Context;
import org.apache.jena.riot.SysRIOT;
import oslomet.noark.xmp.model.MetadataObject;
import oslomet.noark.xmp.model.MetadataObjectDescription;

import javax.validation.constraints.NotNull;
import java.io.InputStream;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import static java.lang.System.out;
import static oslomet.noark.xmp.Constants.*;


public class XMPGetter {

    private PDDocument pdfDoc;

    private Map <String, MetadataObjectDescription> metadataObjectDescriptionMap;

    public XMPGetter(@NotNull PDDocument pdfDoc) {
        this.pdfDoc = pdfDoc;
        metadataObjectDescriptionMap = new HashMap<>();

        // Dublin Core metdata items

        metadataObjectDescriptionMap.put(NS_DC_1_1_CREATOR,
                new MetadataObjectDescription(NS_DC_1_1, CREATOR, XMP_DC));

        metadataObjectDescriptionMap.put(NS_DC_1_1_COVERAGE,
                new MetadataObjectDescription(NS_DC_1_1, COVERAGE, XMP_DC));

        metadataObjectDescriptionMap.put(NS_DC_1_1_DESCRIPTION,
                new MetadataObjectDescription(NS_DC_1_1, DESCRIPTION, XMP_DC));

        metadataObjectDescriptionMap.put(NS_DC_1_1_FORMAT,
                new MetadataObjectDescription(NS_DC_1_1, FORMAT, XMP_DC));

        metadataObjectDescriptionMap.put(NS_DC_1_1_IDENTIFIER,
                new MetadataObjectDescription(NS_DC_1_1, IDENTIFIER, XMP_DC));

        metadataObjectDescriptionMap.put(NS_DC_1_1_SOURCE,
                new MetadataObjectDescription(NS_DC_1_1, SOURCE, XMP_DC));

        metadataObjectDescriptionMap.put(NS_DC_1_1_RIGHTS,
                new MetadataObjectDescription(NS_DC_1_1, RIGHTS, XMP_DC));

        metadataObjectDescriptionMap.put(NS_DC_1_1_TITLE,
                new MetadataObjectDescription(NS_DC_1_1, TITLE, XMP_DC));


        // PDF metadata items
        metadataObjectDescriptionMap.put(NS_PDF_1_3_KEYWORDS,
                new MetadataObjectDescription(NS_PDF_1_3, KEYWORDS, XMP_PDF));

        metadataObjectDescriptionMap.put(NS_PDF_1_3_PRODUCER,
                new MetadataObjectDescription(NS_PDF_1_3, PRODUCER, XMP_PDF));

        // PDFA metadata items
        metadataObjectDescriptionMap.put(NS_PDFA_CONFORMANCE,
                new MetadataObjectDescription(NS_PDFA, CONFORMANCE,
                        XMP_PDFA));

        metadataObjectDescriptionMap.put(NS_PDFA_PART,
                new MetadataObjectDescription(NS_PDFA, PART,
                        XMP_PDFA));

        // PDFX metadata items
        metadataObjectDescriptionMap.put(NS_PDFX_1_3_COPYRIGHT,
                new MetadataObjectDescription(NS_PDFX_1_3, COPYRIGHT,
                        XMP_PDFX));

        // XAP items
        metadataObjectDescriptionMap.put(NS_XAP_1_0_BASE_URL,
                new MetadataObjectDescription(NS_XAP_1_0, BASE_URL,
                        XMP_XAP));

        metadataObjectDescriptionMap.put(NS_XAP_1_0_CREATE_DATE,
                new MetadataObjectDescription(NS_XAP_1_0, CREATE_DATE,
                        XMP_XAP));

        metadataObjectDescriptionMap.put(NS_XAP_1_0_CREATOR_TOOL,
                new MetadataObjectDescription(NS_XAP_1_0, CREATOR_TOOL,
                        XMP_XAP));

        metadataObjectDescriptionMap.put(NS_XAP_1_0_LABEL,
                new MetadataObjectDescription(NS_XAP_1_0, LABEL,
                        XMP_XAP));

        metadataObjectDescriptionMap.put(NS_XAP_1_0_METADATA_DATE,
                new MetadataObjectDescription(NS_XAP_1_0, METADATA_DATE,
                        XMP_XAP));

        metadataObjectDescriptionMap.put(NS_XAP_1_0_MODIFY_DATE,
                new MetadataObjectDescription(NS_XAP_1_0, MODIFY_DATE,
                        XMP_XAP));

        metadataObjectDescriptionMap.put(NS_XAP_1_0_NICKNAME,
                new MetadataObjectDescription(NS_XAP_1_0, NICKNAME,
                        XMP_XAP));

        metadataObjectDescriptionMap.put(NS_XAP_1_0_TITLE,
                new MetadataObjectDescription(NS_XAP_1_0, TITLE,
                        XMP_XAP));

        // XAP mm items
        metadataObjectDescriptionMap.put(NS_XAP_1_0_DOCUMENT_ID_MM,
                new MetadataObjectDescription(NS_XAP_1_0, DOCUMENT_ID,
                        XMP_XAP));

        metadataObjectDescriptionMap.put(NS_XAP_1_0_INSTANCE_ID_MM,
                new MetadataObjectDescription(NS_XAP_1_0, INSTANCE_ID,
                        XMP_XAP));

        metadataObjectDescriptionMap.put(NS_XAP_1_0_MODIFY_DATE_MM,
                new MetadataObjectDescription(NS_XAP_1_0, MODIFY_DATE,
                        XMP_XAP));

    }


    public PDDocumentInformation getDocumentInformation() {
        return pdfDoc.getDocumentInformation();
    }

    public PDMetadata getMetadata() {
        PDDocumentCatalog catalog = pdfDoc.getDocumentCatalog();
        return catalog.getMetadata();
    }


    /**
     * getXMPMetadata
     *
     * Attempt to parse the RDF description (XMP metadata) in the PDF file.
     * The following namespaces will be handled:
     *  http://ns.adobe.com/pdf/1.3/
     *  http://ns.adobe.com/xap/1.0/
     *  http://ns.adobe.com/pdfx/1.3/
     *  http://purl.org/dc/elements/1.1/
     *
     * The rdf description is extracted as a bytestream and converted to a
     * String. After that the string containing the RDF is based to jena that is
     * able to parse and provide an iterator for pulling out information.
     *
     *  Note there are some cases where a predicate is found via another node.
     *  We don't deal with this as the moment. Probably do som recursive call
     *  limited by a
     *
     * @return a map containing the key / value pairs of metadata
     * that was discovered.
     * @throws Exception A multitude of exceptions that the caller has to
     * deal with
     */

    public Map<String, MetadataObject> getXMPMetadata() throws Exception {
        PDDocumentCatalog catalog = pdfDoc.getDocumentCatalog();
        PDMetadata metadata = catalog.getMetadata();
        Map<String, MetadataObject> objectMap = new HashMap<>();

        // No XMP metadata in file
        if (metadata != null) {
            InputStream inputStream = metadata.exportXMPMetadata();
            byte[] bytes = IOUtils.toByteArray(inputStream);
            String rdfString = new String(bytes, StandardCharsets.UTF_8);

            rdfString = rdfString.replaceAll("<\\?xpacket end=\"w\"\\?>", "");
            // <?xpacket begin="﻿" id="W5M0MpCehiHzreSzNTczkc9d"?>
            String s =
                    "<\\?xpacket begin=\"\uFEFF\" id=\"\\w*\"\\?>";
            rdfString =
                    rdfString.replaceAll(s, "");


            String s2 =
                    "<x:xmpmeta xmlns:x=\"adobe:ns:meta/\">";

            String s2a =
                    "<x:xmpmeta xmlns:x=\"adobe:ns:meta/\"\\s* x:xmptk=\"([^\"]|\\\\\")*\">";

            //\s*[x:xmptk="\\w*"] "
            String s3 = "</x:xmpmeta>";
            rdfString = rdfString.replaceAll(s2, "");
            rdfString = rdfString.replaceAll(s2a, "");


            rdfString = rdfString.replaceAll(s3, "");

            Map<String, Object> properties = new HashMap<>();
            properties.put("WARN_BAD_NAME", "EM_IGNORE");

            Context cxt = new Context();
            cxt.set(SysRIOT.sysRdfWriterProperties, properties);

            Model model = ModelFactory.createDefaultModel();

            // Build and run a parser
            RDFParser.create()
                    .lang(Lang.RDFXML)
                    .source(new StringReader(rdfString))
                    .context(cxt)
                    .parse(model.getGraph());

            StmtIterator iterator = model.listStatements();


            while (iterator.hasNext()) {
                Statement statement = iterator.next();

                // Get predicate
                Property predicate = statement.getPredicate();

                MetadataObjectDescription metadataObjectDescription =
                        metadataObjectDescriptionMap.get(predicate.
                                toString().toLowerCase());

                if (metadataObjectDescription != null) {

                    objectMap.put(predicate.toString(), new
                            MetadataObject(
                            metadataObjectDescription.getName(),
                            statement.getObject().toString(),
                            metadataObjectDescription.getType()
                            )
                    );
                } else {
                    out.println("Error handling " + predicate.toString());
                }
            }
        }
        else {
            out.println("This PDF file does not contain any XMP metadata");
        }
        return objectMap;
    }
}
