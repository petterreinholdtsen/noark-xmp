package oslomet.noark.xmp.controller;

import javafx.scene.control.TreeItem;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import oslomet.noark.xmp.XMPGetter;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static java.lang.System.err;
import static java.lang.System.out;

/**
 * Created by tsodring on 14/03/18.
 */
public abstract class MetadataController extends TreeViewController {

    public MetadataController(String rootNodeName, boolean expanded) {
        super(rootNodeName, expanded);
    }

    abstract public void processPDFDocument(File selectedDirectory) throws
            Exception;
}
