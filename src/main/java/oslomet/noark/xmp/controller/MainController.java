package oslomet.noark.xmp.controller;

import javafx.fxml.FXML;
import javafx.scene.control.TreeView;
import javafx.stage.FileChooser;
import oslomet.noark.xmp.run.App;

import java.io.File;
import java.io.IOException;


/**
 * Created by tsodring on 14/03/18.
 */

public class MainController {

    private App mainApp;

    /**
     * used to parse the PDF file
     */
    PDFMetadataController pdfMetadataController;
    XMPMetadataController xmpMetadataController;

    @FXML
    TreeView<String> pdfMetadataTree;

    @FXML
    TreeView<String> xmpMetadataTree;

    private File selectedFile;

    public MainController() {
    }

    public void processPDFDocument(File selectedFile) throws Exception {


        pdfMetadataController = new PDFMetadataController("pdf", true);
        pdfMetadataController.processPDFDocument(selectedFile);

        xmpMetadataController = new XMPMetadataController("xmp", true);
        xmpMetadataController.processPDFDocument(selectedFile);

        pdfMetadataTree.setRoot(pdfMetadataController.getRoot());
        pdfMetadataTree.setShowRoot(true);

        xmpMetadataTree.setRoot(xmpMetadataController.getRoot());
        xmpMetadataTree.setShowRoot(true);

        // pick up all contents from the selected directory
        //directoryController.startDirectoryTraversal(selectedFile);
    }


    /**
     * Retrieves the file to process
     * @throws IOException
     */
    public void handleOpenFile() throws Exception {

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Angi PDF fil som skal analyseres");

        
        File selectedFile = fileChooser.showOpenDialog(mainApp.getMainStage());

        if (selectedFile == null) {
            System.out.println("No file chosen");
            return;
        }

        System.out.println("You chose " + selectedFile.toString());
        if (selectedFile.canRead() == true) {
            //Pop up alert saying you cannot read this directory
        }

        processPDFDocument(selectedFile);
    }

    public void setMainApp(App mainApp) {
        this.mainApp = mainApp;
    }
}
