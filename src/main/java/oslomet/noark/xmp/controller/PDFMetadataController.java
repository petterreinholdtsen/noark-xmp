package oslomet.noark.xmp.controller;

import javafx.scene.control.TreeItem;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import oslomet.noark.xmp.XMPGetter;

import java.io.File;
import java.util.*;

import static java.lang.System.err;
import static java.lang.System.out;

/**
 * Created by tsodring on 14/03/18.
 */
public class PDFMetadataController extends TreeViewController {

    public PDFMetadataController(String rootNodeName, boolean expanded) {
        super(rootNodeName, expanded);
    }

    public void processPDFDocument(File selectedDirectory) throws Exception {
        processPDFMetadata(selectedDirectory, root);
    }

    private void processPDFMetadata(File selectedFile,
                                    TreeItem<String> currentNode)
            throws Exception {


        Map<String, String> pdfMetadataMap = new HashMap<>();

        PDDocument pdfDoc = PDDocument.load(selectedFile);

        if (pdfDoc.isEncrypted()) {
            out.println("Cannot process document as it is encrypted!");
            return;
        }

        try {

            XMPGetter xmpGetter = new XMPGetter(pdfDoc);

            out.println("The following Document Information metadata was " +
                    "found.");

            String format = "%-45s%s%n";

            // Get and print out PDF-defined metadata
            PDDocumentInformation info = xmpGetter.getDocumentInformation();
            Set<String> keys = info.getMetadataKeys();

            for (String key : keys) {
                out.printf(format, "[PDF]\t  " + key, ": \t" + info
                        .getCustomMetadataValue(key));
                pdfMetadataMap.put(key, info.getCustomMetadataValue(key));

                TreeItem<String> item = new TreeItem<>(key);
                currentNode.getChildren().add(item);
                item.getChildren().add(new TreeItem<>(info.getCustomMetadataValue(key)));
                item.setExpanded(true);
            }
        } catch (Exception e) {
            out.println("Error when processing metadata in " + selectedFile.toString());
            e.printStackTrace();
        } finally {
            if (pdfDoc != null) {
                try {
                    pdfDoc.close();
                } catch (Throwable err2) {
                    err.println();
                }
            }
        }
    }
}
