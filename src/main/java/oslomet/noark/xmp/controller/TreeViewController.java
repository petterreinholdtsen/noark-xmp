package oslomet.noark.xmp.controller;

import javafx.scene.control.TreeItem;


/**
 * Created by tsodring on 14/03/18.
 */
public class TreeViewController {

    protected TreeItem<String> root;
    protected boolean expanded;

    public TreeViewController(String rootNodeName, boolean expanded) {
        root = new TreeItem <> ();
        root.setValue(rootNodeName);
        root.setExpanded(expanded);
    }

    public TreeItem<String> getRoot() {
        return root;
    }

    protected TreeItem<String> makeBranch(String filename, TreeItem<String> parent) {
        TreeItem<String> item = new TreeItem<>(filename);
        parent.getChildren().add(item);
        return item;
    }

    protected String traverseTowardsParent(String currentString, TreeItem <String> item) {

        if (item.getParent() != null) {
            currentString = traverseTowardsParent(currentString, item.getParent());
        }
        else {
            // We don't want to include the root node, this ignores it
            return currentString;
        }
        return currentString + "/" + item.getValue();
    }
}
