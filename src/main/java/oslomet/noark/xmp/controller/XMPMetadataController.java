package oslomet.noark.xmp.controller;

import javafx.scene.control.TreeItem;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import oslomet.noark.xmp.XMPGetter;
import oslomet.noark.xmp.model.MetadataObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static java.lang.System.err;
import static java.lang.System.out;

/**
 * Created by tsodring on 14/03/18.
 */
public class XMPMetadataController extends MetadataController {

    public XMPMetadataController(String rootNodeName, boolean expanded) {
        super(rootNodeName, expanded);
    }

    public void processPDFDocument(File selectedFile) throws Exception {
        processXMPMetadata(selectedFile, root);
    }

    public Map<String, String> processXMPMetadata(File selectedFile,
                                                  TreeItem<String> currentNode)
            throws Exception {

        Map<String, String> pdfMetadataMap = new HashMap<>();

        PDDocument pdfDoc = PDDocument.load(selectedFile);

        if (pdfDoc.isEncrypted()) {
            out.println("Cannot process document as it is encrypted!");
            return pdfMetadataMap;
        }

        try {

            XMPGetter xmpGetter = new XMPGetter(pdfDoc);

            out.println("The following Document Information metadata was " +
                    "found.");

            String format = "%-45s%s%n";
            // Get and print out XMP defined metata

            Map<String, MetadataObject> metadataObjectMap = xmpGetter
                    .getXMPMetadata();

            for (Map.Entry entry : metadataObjectMap.entrySet()) {
                MetadataObject currentObject = (MetadataObject) entry.getValue();
                out.printf(format,
                        "[" + currentObject.getType() +
                                "]\t  " + currentObject.getName(),
                        ": \t" + currentObject.getValue()
                );
                pdfMetadataMap.put(currentObject.getName(),
                        currentObject.getValue());
                TreeItem<String> item = new TreeItem<>(currentObject.getName());
                item.setExpanded(true);
                currentNode.getChildren().add(item);
                item.getChildren().add(new TreeItem<>(currentObject.getValue()));


            }

        } catch (Exception e) {
            out.println("Error when processing metadata in " + selectedFile.toString());
            e.printStackTrace();
        } finally {
            if (pdfDoc != null) {
                try {
                    pdfDoc.close();
                } catch (Throwable err2) {
                    err.println();
                }
            }
        }
        return pdfMetadataMap;
    }
}
