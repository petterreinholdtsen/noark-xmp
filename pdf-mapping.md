Metadata field mapping between Noark 5 and PDF XMP
==================================================

Here is a mapping proposal between Noark 5 metadata fields
and PDF XMP metadata fields, in other words, how to store
Noark 5 metadata in a PDF.

 ------------------------------  ---------------------------------
               Noark 5 metadata  PDF/XMP metadata
 ------------------------------  ---------------------------------
          dokumentobjekt.tittel  dc:title
 ------------------------------  ---------------------------------

Note, the dc: prefix indicate the field belong to the Dublic Core
metadata set.

These are probably left to map.  Found in Main.java.

     * [PDF]           PDF Version                     : 1.4
     * [PDF]           Linearized                      : No
     * [PDF]           Page Count                      : 1
     * [PDF]           Language                        : en-GB
     * [PDF]           Tagged PDF                      : Yes
     * [PDF]           Creator                         : Writer
     * [PDF]           Producer                        : LibreOffice 5.4
     * [PDF]           Create Date                     : 2018:03:02 09:55:12+01:00
     * [XMP-pdf]       Keywords                        : XMP Metadata Noark-XMP
     * [XMP-pdf]       Producer                        : noark-xmp edit tool producer
     * [XMP-rdf]       About                           : Testing noark-xmp
     * [XMP-xmp]       Base URL                        : https://github.com/HiOA-ABI/noark-xmp
     * [XMP-xmp]       Create Date                     : 2018:03:02 10:59:44+01:00
     * [XMP-xmp]       Creator Tool                    : noark-xmp edit tool creator
     * [XMP-xmp]       Label                           : noark-xmp label
     * [XMP-xmp]       Metadata Date                   : 2018:03:02 10:59:44+01:00
     * [XMP-xmp]       Modify Date                     : 2018:03:02 10:59:44+01:00
     * [XMP-xmp]       Nickname                        : noark-xmp nickname
     * [XMP-xmp]       Title                           : Testing adding metadata
     * [XMP-dc]        Title                           : dc: title
     * [XMP-dc]        Creator                         : dc: creator
     * [XMP-dc]        Description                     : dc: description
     * [XMP-dc]        Coverage                        : dc: coverage
     * [XMP-dc]        Format                          : dc: format
     * [XMP-dc]        Rights                          : dc: rights
     * [XMP-dc]        Identifier                      : dc: identifier
     * [XMP-dc]        Source                          : dc: source

See also work done to map Noark 5 to RDF, available from
https://github.com/SesamResearch/Records-Management-and-Archive-Systems-Research

